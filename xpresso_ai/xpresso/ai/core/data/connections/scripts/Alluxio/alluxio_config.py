"""Module to specify configurations for setting up Alluxio locally"""

# ALLUXIO CONFIG
# To know more about how to write properties for setting up an under-storage visit,
# https://docs.alluxio.io/os/user/stable/en/Overview.html

alluxio_properties = "\n\n# COMMON CONFIG" \
                     "\nalluxio.master.hostname=localhost\n" \
                     "# alluxio.user.file.writetype.default=CACHE_THROUGH" \
                     "\n\n# NFS Config" \
                     "\n# alluxio.underfs.address=/home/xprops/alluxio_data" \
                     "\n\n# HDFS Config" \
                     "\n# alluxio.underfs.address=hdfs://10.0.23.22:8020/user/abzadmin/avani/Input\n" \
                     "# alluxio.underfs.hdfs.configuration=/etc/hadoop/conf/core-site.xml," \
                     "/etc/hadoop/conf/hdfs-site.xml" \
                     "\n\n# S3 Config" \
                     "\n# alluxio.underfs.address=s3a://xpresso-commerce/\n" \
                     "# aws.accessKeyId=AKIAROVYJM2RSX7CE252\n" \
                     "# aws.secretKey=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" \
                     "\n\n# GCS Config" \
                     "\n# alluxio.underfs.address=gs://<GCS_BUCKET>/<GCS_DIRECTORY>\n" \
                     "# fs.gcs.accessKeyId=<GCS_ACCESS_KEY_ID>\n" \
                     "# fs.gcs.secretAccessKey=<GCS_SECRET_ACCESS_KEY>" \
                     "\n\n# Azure Config" \
                     "\n# alluxio.underfs.address=wasb://<AZURE_CONTAINER>@<AZURE_ACCOUNT>" \
                     ".blob.core.windows.net/<AZURE_DIRECTORY>/\n" \
                     "# alluxio.master.mount.table.root.option.fs.azure.account.key.<AZURE_ACCOUNT>" \
                     ".blob.core.windows.net=<YOUR ACCESS KEY>"

